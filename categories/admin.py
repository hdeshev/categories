from categories.models import *
from django.contrib import admin

for model in [Category, Product, Customer, Order, OrderItem]:
    admin.site.register(model)
