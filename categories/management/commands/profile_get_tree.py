from __future__ import absolute_import, division, print_function, unicode_literals

from django.core.management.base import BaseCommand
from django.db import connection
from categories.models import get_category_tree
import cProfile


class Command(BaseCommand):
    args = '<none>'
    help = 'Profiles the get_category_tree function'

    def handle(self, *args, **options):
        cProfile.run("from categories.models import get_category_tree; get_category_tree()")

        for query in connection.queries:
            print(query)

        print("{} SQL queries.".format(len(connection.queries)))

        self.stdout.write('profile done!')
