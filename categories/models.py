# -*- coding: utf-8 -*-

# text moved to specs.txt

import random
import decimal
import sys

from django.db import models
from django.db.models import F
from django.db.models import Count
from django.db import transaction


class Category(models.Model):
    name = models.CharField(max_length=255)
    parent = models.ForeignKey('self', null=True, blank=True)

    def __repr__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=255)
    retail_price = models.DecimalField(max_digits=8, decimal_places=2)
    cost_price = models.DecimalField(max_digits=8, decimal_places=2)
    category = models.ForeignKey(Category)

    def __repr__(self):
        return self.name

class Customer(models.Model):
    name = models.CharField(max_length=255)

class Order(models.Model):
    customer = models.ForeignKey(Customer)
    date = models.DateTimeField("Purchase timestamp", auto_now=True)

class OrderItem(models.Model):
    quantity = models.PositiveIntegerField()
    product = models.ForeignKey(Product)
    order = models.ForeignKey(Order)


CATEGORY = 0
CHILDREN = 1


class TreeItem(object):
    def __init__(self, category):
        self.category = category
        self.products = []
        self.children = []

    def add_product(self, product):
        self.products.append(product)

    def add_child(self, child_item):
        self.children.append(child_item)

    def not_empty(self):
        return len(self.child_tuples()) > 0 or len(self.products) > 0

    def child_tuples(self):
        return [child.to_tuple() for child in self.children if child.not_empty()]

    def to_tuple(self):
        sorted_children = sorted(self.child_tuples(), key=lambda child: child[0])
        sorted_products = sorted(self.products, key=lambda product: product.name)
        return (self.category, sorted_children + sorted_products)


def get_category_tree():
    category_list = Category.objects.all().prefetch_related("parent")
    products = Product.objects.all().prefetch_related("category")

    items = {}

    for category in category_list:
        item = items.get(category.pk)
        if not item:
            item = TreeItem(category)
            items[category.pk] = item

    for product in products:
        item = items[product.category.pk]
        item.add_product(product)

    for item in items.values():
        if item.category.parent is not None:
            items[item.category.parent.pk].add_child(item)

    root_item = TreeItem(None)
    for toplevel in [item for item in items.values() if item.category.parent is None]:
        root_item.add_child(toplevel)

    return root_item.to_tuple()

def get_products_with_low_profit_margin(amount_in_percentage):
    low_profit = Product.objects.filter(retail_price__lt=F("cost_price") * (1 + amount_in_percentage))
    with_profit_column = low_profit.extra(select={'profit': "retail_price - cost_price"})
    sorted_by_profit = with_profit_column.extra(order_by = ['profit'])
    return sorted_by_profit


def get_list_of_orders_without_items():
    return Order.objects.annotate(order_items=Count('orderitem')).filter(order_items=0)


def get_suggestions_of_a_product(customer):
    #####
    # Finish me ( Problem 5 )
    ####
    pass


def sort_categories_by_profit(category_tree, cache=None):
    quantities = {
        r['product_id'] : r['product__count'] for r in
        OrderItem.objects.values('product_id').annotate(models.Count('product'))
    }

    if cache is None:
        cache = {}

    def profit_function(node):
        if isinstance(node, tuple):
            cat = node[CATEGORY]
            profit = cache.get(cat)
            if profit is None:
                profit = sum(map(profit_function, node[CHILDREN]))
                cache[cat] = profit

            return profit
        else:
            return max(node.retail_price - node.cost_price, 0) * quantities.get(node.id, 0)

    # We use a cache to reduce the duplication of effort, but a better solution
    # would use dynamic programing and a depth first iteration of the tree to calculate
    # the profit bottom-up
    _sort_node_by_profit(category_tree, profit_function)

def _sort_node_by_profit(node, profit_function):
    node[CHILDREN].sort(key=profit_function)
    for node in node[CHILDREN]:
        if isinstance(node, tuple):
            _sort_node_by_profit(node, profit_function)

def rand_money(lb, ub):
    amt = float(lb) + random.random() * float(ub - lb)
    return decimal.Decimal(str(round(amt, 2)))

@transaction.commit_on_success
def generate_data(**kwargs):
    if kwargs.get("app") and kwargs.get('app').__file__ != __file__:
        return

    print('Generating data.')

    names = ['Foo', 'Bar', 'Zoo', 'Baz', 'Alpha', 'Bravo', 'Charlie', 'Delta', 'Epsilon', 'Lorem', 'Dolor', 'Ipsum', 'Sit', 'Amet']

    OrderItem.objects.all().delete()
    Order.objects.all().delete()
    Product.objects.all().delete()
    Category.objects.all().delete()
    Customer.objects.all().delete()


    customers = []
    categories = []

    for name in names:
        cat = Category.objects.create(name=name + ' Category')
        categories.append(cat)

        cust = Customer.objects.create(name=name + ' Customer')
        customers.append(cust)

    for cat in categories[4:]:
        while True:
          cat.parent = random.choice(categories)
          if cat.parent is not cat:
            break

        cat.save()

    products = []
    for first in names:
        for last in names:
            cost_price = rand_money(10, 1000)
            retail_price = rand_money(cost_price, cost_price*2)

            product = Product.objects.create(name=first + ' ' + last + ' Widget', retail_price=retail_price, cost_price=cost_price, category=random.choice(categories))
            products.append(product)

    orders = []
    for i in range(1,1000):
        anOrder = Order.objects.create(customer=random.choice(customers))
        orders.append(anOrder)


    for prod in products:
        quantity = int(round(random.random() + 0.6))
        for i in xrange(random.randint(0, 10)):
            OrderItem.objects.create(quantity=quantity, product=random.choice(products), order=random.choice(orders))

    Category.objects.create(name='Brand new Category')

    print('Finished generating data.')

from django.db.models.signals import post_syncdb

if not hasattr(sys, "_called_from_test"):
    post_syncdb.connect(generate_data)
