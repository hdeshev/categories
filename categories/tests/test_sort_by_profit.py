from __future__ import absolute_import, division, print_function, unicode_literals
from .test_category_tree import tree
from categories.models import Category, sort_categories_by_profit
import pytest
from pprint import pprint
from copy import deepcopy


@pytest.mark.django_db
def test_sort_by_profit(tree):
    tree_copy = deepcopy(tree)
    test_cache = {}
    sort_categories_by_profit(tree_copy, test_cache)

    def assert_category(cat):
        category, children = cat
        if category is not None:
            profit = test_cache[category]
            assert profit >= 0


        products = [(index, child) for (index, child) in enumerate(children) if type(child) is not tuple]
        categories = [(index, child) for (index, child) in enumerate(children) if type(child) is tuple]
        for category in categories:
            assert_category(category[1])

        # categories come first
        product_indexes = [index for (index, item) in products]
        category_indexes = [index for (index, item) in categories]
        assert max(category_indexes + [-1]) < min(product_indexes + [10000000])

    assert_category(tree)
