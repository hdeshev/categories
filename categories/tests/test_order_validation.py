from __future__ import absolute_import, division, print_function, unicode_literals
from categories.models import Category, get_list_of_orders_without_items, generate_data
import pytest

def setup():
    generate_data()

@pytest.mark.django_db
def test_orders_without_items():
    no_items = get_list_of_orders_without_items()
    assert len(no_items) > 0

# (0.000) SELECT "categories_order"."id", "categories_order"."customer_id", "categories_order"."date",
#        COUNT("categories_orderitem"."id") AS "order_items"
#    FROM "categories_order" LEFT OUTER JOIN "categories_orderitem" ON
#        ("categories_order"."id" = "categories_orderitem"."order_id")
#    GROUP BY "categories_order"."id", "categories_order"."customer_id", "categories_order"."date"
#    HAVING COUNT("categories_orderitem"."id") = 0 ; args=(0,)
