from __future__ import absolute_import, division, print_function, unicode_literals
from categories.models import Category, get_products_with_low_profit_margin, generate_data
import pytest

def setup():
    generate_data()

@pytest.mark.django_db
def test_product_profit_margin():
    low_profit = get_products_with_low_profit_margin(0.1)
    assert len(low_profit) > 0

# >>> get_products_with_low_profit_margin(0.1)
# (0.001) SELECT (retail_price - cost_price) AS "profit", "categories_product"."id",
#    "categories_product"."name", "categories_product"."retail_price", "categories_product"."cost_price",
#    "categories_product"."category_id"
#    FROM "categories_product"
#    WHERE "categories_product"."retail_price" <  "categories_product"."cost_price" * 1.1
#    ORDER BY "profit" ASC LIMIT 21; args=(1.1,)
