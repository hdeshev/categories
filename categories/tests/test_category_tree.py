from __future__ import absolute_import, division, print_function, unicode_literals
from categories.models import Category, get_category_tree, generate_data
import pytest
from pprint import pprint

@pytest.mark.django_db
@pytest.fixture(scope="session")
def tree():
    generate_data()
    return get_category_tree()


@pytest.mark.django_db
def test_get_categories(tree):
    category, children = tree
    assert category is None
    assert len(children) > 0


@pytest.mark.django_db
def test_toplevel_has_categories_only(tree):
    for toplevel in tree[1]:
        assert toplevel[0] is not None and toplevel[0].name.endswith("Category")


@pytest.mark.django_db
def test_tree_structure_is_valid(tree):
    def assert_category(cat):
        category, children = cat
        assert category is None or category.name.endswith("Category")

        products = [(index, child) for (index, child) in enumerate(children) if type(child) is not tuple]
        categories = [(index, child) for (index, child) in enumerate(children) if type(child) is tuple]
        for category in categories:
            assert_category(category[1])

        # categories come first
        product_indexes = [index for (index, item) in products]
        category_indexes = [index for (index, item) in categories]
        assert max(category_indexes + [-1]) < min(product_indexes + [10000000])

    assert_category(tree)
