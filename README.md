# Installing prerequisites

Assuming you use virtualenvwrapper:

    mkvirtualenv categories
    workon categories
    
    pip install -r requirements.txt

# Running the tests:

    py.test
